import logging
import turtle as tr
import math


logger = logging.getLogger(__name__)

screen = tr.Screen()

default_timer = 80


class BaseActor(object):
    tur = None

    def __init__(self, shape_name, x, y):
        tur = self.tur = tr.Turtle(visible=False)
        tur.shape(shape_name)
        tur.penup()
        tur.speed(0)
        tur.color("green")
        tur.goto(x, y)


class Bullet(BaseActor):
    y_limit = tr.window_height() / 2 - 50

    def __init__(self, x, y, bpool, invader_army):
        """
        :param x:
        :param y:
        """
        screen.register_shape("bullet", ((0, 0), (0, 10), (2, 10), (2, 0)))
        super(Bullet, self).__init__("bullet", x, y)
        self.tur.left(90)
        self.bpool = bpool
        self.invader_army = invader_army

    def advance(self):
        if self.tur.ycor() < self.y_limit:
            self.tur.forward(25)
            self.invader_army.invader_and_bullet_collision([self])
            # if this bullet has hit a target it will not be visible and we have to
            # make sure not to schedule another advance call as it can hit another target on next step
            if self.tur.isvisible():
               screen.ontimer(self.advance, default_timer)
        else:
            # bullet is leaving the screen
            self.bpool.deactivate(self)
            self.tur.hideturtle()


class BulletPool(object):
    def __init__(self, invader_army):
        self.bullet_to_use_pointer = 0
        self.bullet_pool = []
        for _ in range(20):
            self.bullet_pool.append(Bullet(0, 0, self, invader_army))
        self.invader_army = invader_army
        self.active_bullet_lst = []

    def activate(self):
        b = self.bullet_pool[self.bullet_to_use_pointer]
        self.bullet_to_use_pointer += 1
        self.bullet_to_use_pointer = self.bullet_to_use_pointer % len(self.bullet_pool)
        self.active_bullet_lst.append(b)
        return b

    def deactivate(self, b):
        self.active_bullet_lst.remove(b)


class Ship(BaseActor):

    def __init__(self, x, y, bpool):
        screen.register_shape("ARC-170", ((10, -10), (5, 0), (10, 10), (-10, 0)))
        super(Ship, self).__init__("ARC-170", x, y)
        self.tur.showturtle()
        self.bpool = bpool

    def right(self):
        logger.info('right')
        limit = tr.window_width() / 2 - 50
        if self.tur.xcor() < limit:
            self.tur.forward(10)
            self.bpool.invader_army.ship_and_invader_collision(self)
        self.ship_diags()

    def left(self):
        logger.info('left')
        limit = -tr.window_width() / 2 + 50
        if self.tur.xcor() > limit:
            self.tur.backward(10)
            self.bpool.invader_army.ship_and_invader_collision(self)
        self.ship_diags()

    def shoot(self):
        logger.info('shoot')
        bullet = self.bpool.activate()
        bullet.tur.goto(self.tur.xcor(), self.tur.ycor() + 15)
        bullet.tur.showturtle()
        bullet.advance()

    def ship_diags(self):
        logger.info('Ship at x: {}, y: {}'.format(
            self.tur.xcor(), self.tur.ycor())
        )


class InvaderArmy(object):
    invader_list = None
    x_speed = 10

    def __init__(self):
        screen.register_shape("invader", ((10, -10), (10, 10), (-10, 10), (-10, -10)))
        self.invader_list = []  # list()
        for y in range(3):
            for x in range(5):
                tur = self._make_invader(x, y)
                self.invader_list.append(tur)

    def _make_invader(self, x, y):
        tur = tr.Turtle(visible=False)
        tur.shape("invader")
        tur.penup()
        tur.speed(0)
        tur.color("green")
        x_start = - tr.window_width() / 2 + 50
        y_start = tr.window_height() / 2 - 150
        tur.goto(x_start + x * 100, y_start - y * 50)
        tur.showturtle()
        return tur

    def move(self):
        # can i move? do i need to invert speed and move down?
        ystep = 0
        left_inv_x = self.invader_list[0].xcor()
        right_inv_x = self.invader_list[-1].xcor()
        absolute_limit = abs(tr.window_width() / 2) - 50
        # if we hit left limit or right limit
        if abs(left_inv_x) > absolute_limit or \
                abs(right_inv_x) > absolute_limit:
            # invert speed and request y move
            self.x_speed *= -1
            ystep = -50

        # move the army
        for invader in self.invader_list:
            x = invader.xcor()
            x += self.x_speed
            invader.setx(x)

            y = invader.ycor()
            y += ystep
            invader.sety(y)

    @staticmethod
    def _is_collision(tur1, tur2, col_distance=15):
        dx = tur1.xcor() - tur2.xcor()
        dy = tur1.ycor() - tur2.ycor()
        distance = math.sqrt(math.pow(dx, 2) + math.pow(dy, 2))
        return distance < col_distance

    def game_won(self):
        for invader in self.invader_list:
            if invader.isvisible():
                # some of them still alive
                return False
        return True

    def ship_and_invader_collision(self, ship):
        for inv in self.invader_list:
            if self._is_collision(inv, ship.tur, col_distance=50):
                # collision happened
                tr.color("green")
                style = ("Courier", 50, "italic")
                tr.write("Game Over!", font=style, align="center")
                tr.exitonclick()

    def invader_and_bullet_collision(self, active_bullet_lst):
        for bullet in active_bullet_lst:
            for inv in self.invader_list:
                if self._is_collision(inv, bullet.tur) and \
                        inv.isvisible() and bullet.tur.isvisible():
                    # collision happened
                    inv.hideturtle()
                    active_bullet_lst.remove(bullet)
                    bullet.tur.hideturtle()
                    if self.game_won():
                        tr.color("green")
                        style = ("Courier", 50, "italic")
                        tr.write("Victory!", font=style, align="center")
                        tr.exitonclick()

