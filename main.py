import logging

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)

import turtle as tr
tr.setup(1280, 880)
logging.info("Using window width, height: {}, {}".format(tr.window_width(), tr.window_height()))

from objects import Ship, screen, InvaderArmy, BulletPool, default_timer

logger.info('off we go')

screen.bgcolor("black")



invader_army = InvaderArmy()
bpool = BulletPool(invader_army)
ship = Ship(0, -(tr.window_height() / 2 - 50), bpool)

tr.onkey(ship.right, "Right")
tr.onkey(ship.left, "Left")
tr.onkey(ship.shoot, "space")
tr.listen()


##########################################################
# collision handling follows move of each object
def invader_army_on_timer():
    # advance army
    invader_army.move()
    # check collisions
    invader_army.invader_and_bullet_collision(bpool.active_bullet_lst)
    invader_army.ship_and_invader_collision(ship)
    # schedule next call
    screen.ontimer(invader_army_on_timer, default_timer)
invader_army_on_timer()
#
# further collisions handling is implemented along the position impacting
# actions in: bullet.advance, ship.left and ship.right methods
##########################################################


screen.mainloop()